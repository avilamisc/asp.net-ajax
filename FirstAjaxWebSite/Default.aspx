﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>First Asp.Net Ajax</title>
    
</head>
<body>
  <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <script type="text/javascript">

          //window.onload = function () {
          //    var refButton = document.getElementById("btnButton");

          //    refButton.onclick = function () {
          //        alert('I am clicked!');
          //    }
          //};

          window.onload = function () {
              var s = "";

              Type.registerNamespace("OReilly");
              OReilly.Software = function (name, vendor) {
                  var _name = (name != null) ? name : "unknown";
                  var _vendor = (vendor != null) ? vendor : "unknown";

                  this.getName = function () {
                      return _name;
                  }
                  this.setName = function (name) {
                      _name = name;
                  }

                  this.getVendor = function () {
                      return _vendor;
                  }
                  this.setVendor = function (vendor) {
                      _vendor = vendor;
                  }
              }

              //Type.registerClass("OReilly.Software");
              OReilly.Software.registerClass("OReilly.Software");

              var ie = new OReilly.Software("Internet Explorer", "Microsoft");
              s = ie.getName() + " from " + ie.getVendor() + "<br />";

              var ff = new OReilly.Software();
              ff.setName("Firefox");
              ff.setVendor("Mozilla Foundation");
              s += ff.getName() + " from " + ff.getVendor();

              document.getElementById("div1").innerHTML = s;
              //var d = document.childNodes.length;
              //var elem = $get("div1");

              //elem.innerHTML = s;
          };

          

  </script>
    <div id="div1">
    </div>
  </form>
</body>
</html>
<%--<form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/WebService.asmx" />
        </Services>
    </asp:ScriptManager>
        <div>
            <input type="text" id="Name" name="TextBox1"/>
            <input type="button" value="Show Text" onclick="ShowText();" />
            <p>Entered text: </p>
        </div>
        <div id="output"></div>

    </form>--%>